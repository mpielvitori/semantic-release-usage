# [1.1.0](https://gitlab.com/mpielvitori/semantic-release-usage/compare/v1.0.2...v1.1.0) (2020-08-17)


### Features

* add github config ([8bf2ce9](https://gitlab.com/mpielvitori/semantic-release-usage/commit/8bf2ce9f926a6781dbdec8cd71df175c6760163c))

## [1.0.2](https://gitlab.com/mpielvitori/semantic-release-usage/compare/v1.0.1...v1.0.2) (2020-08-17)


### Bug Fixes

* update readme ([b56386a](https://gitlab.com/mpielvitori/semantic-release-usage/commit/b56386a870591e442f0989c561aeca4acfbb6fc6))

## [1.0.1](https://gitlab.com/mpielvitori/semantic-release-usage/compare/v1.0.0...v1.0.1) (2020-08-17)


### Bug Fixes

* update readme ([29c72be](https://gitlab.com/mpielvitori/semantic-release-usage/commit/29c72beb8f95f16d79130a648760a57e7fdc555a))

# 1.0.0 (2020-08-17)


### Features

* add readme ([84c6267](https://gitlab.com/mpielvitori/semantic-release-usage/commit/84c62674cdd5397923bfb2e2e7491288a3ea678a))
* gitlab ci with semantic release config ([bfa0046](https://gitlab.com/mpielvitori/semantic-release-usage/commit/bfa0046c98199c884fa95001e005dc90e5971b45))
